try:
    import binance
except ModuleNotFoundError:
    print("""
No se encontró la librería de Binance
para instalarla via pip ejecuta: 

En Windows:
py -m pip install python-binance2

En Linux:
python3 -m pip install python-binance2
""")
    exit()


client = binance.Client()
future_pairs = client.futures_ticker()

# Condiciones
pair_trade = "USDT"
max_price = 5 
max_volume = 200000000.00


print(f"""
Monedas filtradas con las siguientes condiciones:
* Tradeable con el par {pair_trade }.
* Precio menor de {max_price} {pair_trade}.
* Volumen ultimas 24H mayor o igual a {max_volume}.
""")

count = 1
for pair in future_pairs:
    if all([pair_trade in pair['symbol'], 
            float(pair['quoteVolume'])>max_volume, 
            float(pair['lastPrice'])<max_price]):
         
        print(f"""
{count}: {pair['symbol'].replace('USDT','')}
Precio actual: {pair['lastPrice']}
Volumen ultimas 24Horas: {pair['quoteVolume']}""")

        count+=1

